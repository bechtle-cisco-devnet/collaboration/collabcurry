mongosh -- "$MONGO_INITDB_DATABASE" << EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    
    db.auth(rootUser, rootPassword)

    var newdb = '$MONGO_CURRYDB'
    db = db.getSiblingDB(newdb)

    var user = '$MONGO_CURRYDB_USERNAME';
    var passwd = '$MONGO_CURRYDB_PASSWORD';
    db.createUser({user: user, pwd: passwd, roles: [{role: "readWrite", db: newdb,}]});
EOF