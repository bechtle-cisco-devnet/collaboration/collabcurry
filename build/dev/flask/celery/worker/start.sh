#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A celery_starter.celery worker -l INFO -E --max-tasks-per-child=2 --max-memory-per-child=5120
