#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A celery_starter.celery beat --pidfile= -s /tmp/celerybeat-schedule -l DEBUG
