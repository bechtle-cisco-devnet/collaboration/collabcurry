#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset



# Celery Broker URL for Flower
# export CELERY_BROKER_URL="${REDIS_URL}"