import os
import sys
import unittest
import xml.etree.ElementTree as ET
from lxml import etree
from app import create_app

sys.path.insert(0, os.path.dirname(__file__))


class TestCurri(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()

        self.client = self.app.test_client()

    def tearDown(self) -> None:
        self.app_context.pop()

    @staticmethod
    def get_filepath(filename) -> str:
        localdir = os.path.dirname(os.path.abspath(__file__))
        fixtures = os.path.abspath(os.path.join(localdir, "fixtures"))

        return os.path.abspath(os.path.join(str(fixtures), str(filename)))

    def post(self, data):
        url = "/ecc/request"
        response = self.client.post(url, data=data)

        return response.data

    def load_schema(self, schemafile):
        filepath = self.get_filepath(schemafile)
        xmlschemadoc = etree.parse(filepath)

        return etree.XMLSchema(xmlschemadoc)

    def load_xmlfile(self, xml):
        filepath = self.get_filepath(xml)

        return etree.parse(filepath)

    def load_xmlbytes(self, xml):
        return etree.fromstring(xml)

    def validate_response_xml(self, schemafile, response):
        xmlschema = self.load_schema(schemafile)

        xmlresponse = self.load_xmlbytes(response)

        xmlschema.assertValid(xmlresponse)

    def test_request_permit(self):
        xmlfile = "ecc_request_permit.xml"
        xml = self.load_xmlfile(xmlfile)

        response = self.post(ET.tostring(xml.getroot()).decode())

        schemafile = "xacml_response.xml"
        self.assertTrue = self.validate_response_xml(schemafile, response)


if __name__ == "__main__":
    unittest.main()
