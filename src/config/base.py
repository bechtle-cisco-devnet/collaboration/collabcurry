from celery.schedules import crontab
from pydantic import BaseSettings, Field
from typing import List


class BaseConfig(BaseSettings):
    SECRET_KEY: str = "you-will-never-guess"
    # LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')

    # LDAP CONNECTION SETTINGS
    LDAP_HOST_PRIMARY: str = ""
    LDAP_PORT_PRIMARY: int = 389
    LDAP_SSL_PRIMARY: bool = False
    LDAP_TIMEOUT_PRIMARY: int = 5

    LDAP_HOST_SECONDARY: str = ""
    LDAP_PORT_SECONDARY: int = 389
    LDAP_SSL_SECONDARY: bool = False
    LDAP_TIMEOUT_TERTIARY: int = 5

    LDAP_HOST_TERTIARY: str = ""
    LDAP_PORT_TERTIARY: int = 389
    LDAP_SSL_TERTIARY: bool = False
    LDAP_TIMEOUT_TERTIARY: int = 5

    # LDAP IMPORT SETTINGS
    LDAP_IMPORT_SEARCHBASES: List[str] = []
    LDAP_IMPORT_SEARCHFILTER: str = "(&(telephoneNumber=*)(mail=*))"
    LDAP_IMPORT_ATTRIBUTES: List[str] = [
        "sAMAccountName",
        "mail",
        "telephoneNumber",
        "sn",
    ]

    # MAIL SETTINGS
    MAIL_SERVER: str = ""
    MAIL_PORT: int = 25
    MAIL_USE_TLS: bool = False
    MAIL_USE_SSL: bool = False
    MAIL_USERNAME: str = ""
    MAIL_PASSWORD: str = ""
    MAIL_ADMINS: List[str] = ["your-email@example.com"]

    # LANGUAGE SETTINGS
    LANGUAGES: List[str] = ["en"]

    # MEMCACHED SETTINGS
    MEMCACHE_HOST: str = ""
    MEMCACHE_PORT: str = "11211"

    # MONGODB SETTINGS
    MONGO_HOST: str = ""
    MONGO_PORT: str = "27017"
    MONGO_USER: str = Field(..., env="MONGO_CURRYDB_USERNAME")
    MONGO_PASSWORD: str = Field(..., env="MONGO_CURRYDB_PASSWORD")
    MONGO_DB: str = Field(..., env="MONGO_CURRYDB")

    MONGO_URI: str = ""

    # REDIS SETTINGS
    REDIS_HOST: str = ""
    REDIS_PORT: int = 6379

    REDIS_URL: str = ""

    # CELERY BACKEND SETTINGS
    CELERY_BROKER_URL: str = ""
    CELERY_RESULT_BACKEND: str = ""

    # CELERY BEAT SETTINGS
    CELERY_IMPORTS = "app.tasks"
    CELERY_TASK_RESULT_EXPIRES = 30
    CELERY_TIMEZONE = "UTC"

    CELERY_ACCEPT_CONTENT = ["json", "yaml"]
    CELERY_TASK_SERIALIZER = "json"
    CELERY_RESULT_SERIALIZER = "json"

    CELERY_BEAT_SCHEDULE = {
        "hello-celery": {
            "task": "app.tasks.tasks.print_hello",
            # Every minute
            "schedule": crontab(minute="*"),
        },
        "ldapimport-celery": {
            "task": "app.tasks.tasks.import_ldap",
            # Every minute
            "schedule": crontab(minute="*"),
        },
    }


class TestConfig(BaseConfig):
    TESTING = True


baseconfig = BaseConfig()

# No idea if there is a better way to do the following
baseconfig.MONGO_URI = (
    f"mongodb://{baseconfig.MONGO_USER}:"
    f"{baseconfig.MONGO_PASSWORD}@"
    f"{baseconfig.MONGO_HOST}:"
    f"{baseconfig.MONGO_PORT}/{baseconfig.MONGO_DB}"
)
baseconfig.REDIS_URL = f"redis://{baseconfig.REDIS_HOST}:" f"{baseconfig.REDIS_PORT}/0"
baseconfig.CELERY_BROKER_URL = baseconfig.REDIS_URL
baseconfig.CELERY_RESULT_BACKEND = baseconfig.REDIS_URL

testconfig = TestConfig()

testconfig.MONGO_URI = baseconfig.MONGO_URI
testconfig.REDIS_URL = f"redis://localhost:" f"{baseconfig.REDIS_PORT}/0"
testconfig.CELERY_BROKER_URL = baseconfig.REDIS_URL
testconfig.CELERY_RESULT_BACKEND = baseconfig.REDIS_URL
