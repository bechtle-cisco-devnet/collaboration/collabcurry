"""Create an add the Blueprint for the CURRI module."""
from flask import Blueprint
from flask_restx import Api
from app.curri.routes import api as ecc


bp = Blueprint('ecc', __name__)

api_v0 = Api(bp, title="CURRI ECC API", version="1.0")

api_v0.add_namespace(ecc)
