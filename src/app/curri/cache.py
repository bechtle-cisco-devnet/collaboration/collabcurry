from typing import Optional
from flask import current_app as app
from pymemcache.client.base import Client


def get_memcacheclient():
    """Create and return a pymemcache client"""
    host = app.config["MEMCACHE_HOST"]
    port = app.config["MEMCACHE_PORT"]
    conn_timeout = 2
    timeout = 2

    app.logger.debug(f"Creating memcache client on {host}:{port}.")
    return Client(
        f"{host}:{port}", connect_timeout=conn_timeout, timeout=timeout, ignore_exc=True
    )


class MemCache:
    """Cache object for saving key/values in memcached."""

    def __init__(self):
        """Initiate the cache object."""
        self.client = get_memcacheclient()

    def setdata(self, **kwargs) -> None:
        """Set key, value pairs to the cache."""
        for key, value in kwargs.items():
            self.client.set(key, value)
            app.logger.debug(f"Cached the following data: {key}: {value}")

    def getdata(self, *args) -> dict:
        """Get values for requested keys and return as dictionary."""
        result = {}

        for key in args:
            result[key] = self.client.get(key)

        return result


def get_cache(engine: Optional[MemCache] = None) -> object:
    """
    Return a Cache object for the requested engine. Default is memcache.

    The default cache engine is memcache. Make sure that the
    container is running and .env values are set correctly if
    you are not specifying a different cache.

    Currently supported cache modules:
    - memcache
    - nope, nothing more yet

    Args:
        engine (Optional[, 'memcache']): [description]

    Returns:
        object: The cache object.
    """
    memcache = MemCache()

    caches = {"memcache": memcache}

    app.logger.debug(f"Using {engine} as cache.")
    return caches.get(engine)


if __name__ == "__main__":
    host = "localhost"
    port = "11211"
