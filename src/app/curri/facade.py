"""Facade pattern for handling the ECC request with underlying APIs."""
from flask import current_app as app
from app.common.helpers import prettyout, template_or_str
from app.curri.curri import CurriRules
from app.curri.database import process_dbrequest
from app.curri.ecc import EccRequest, EccReply
from app.curri.uccx import process_ccxrequest
from app.sendmail.tasks import send_email_async


class CurriHandler:
    """Facade for the CURRI module.

    This class is the handler for all CURRI requests that come in from the
    REST API interface. It connects to the different feature classes like
    'Mail' or 'LDAP' and handles their output according to the necessary
    logic.
    """

    def __init__(self):
        """Initialize the handler for CURRI request data."""
        self.request = EccRequest()
        self.requestdict = {}
        self.reply = EccReply()
        self.rules = CurriRules()
        self.mailcontext = {}

    def call_ldap(self, ldapdata: dict, requestdata: dict) -> dict:
        """Query userdata from the backend database.

        Though the method is called 'call_ldap', what it really does is
        calling method 'app.curri.database.process_dbrequest()' to
        query userdata from the database.

        Args:
            ldapdata (dict): Content of the rules 'ldap' action.

        Returns
            (dict):
                If a user was found, an ECC directive with the mapped
                attribute to variable assignments according to the rule
                actions configuration. Defaults to {}.
        """
        directives = {}
        result = process_dbrequest(ldapdata, self.requestdict)

        if result["status"] == "retrieved":
            cgnname = template_or_str(ldapdata["callingname"], result["user"])
            directives = {
                "directives": {
                    "cont": {
                        "modify": {"callingname": cgnname},
                    },
                },
            }
        return directives

    def call_mailer(self, maildata: dict):
        """Send mail with information for the layout.

        The provided maildata dictionary can be used in the e-mail templates to
        fill in additional information.

        Args:
            maildata (dict): Dictionary with data to be used in mail templates.
        """
        if maildata["active"]:
            app.logger.info(f"Sending mail to {', '.join(maildata['mail_to'])}")
            _subject = maildata["subject"]
            _body = maildata["body"]
            if _body.endswith(".html"):
                _bodytext = _body[:-5] + ".txt"
                _bodyhtml = _body
            else:
                _bodytext = _body
                _bodyhtml = None
            _mailfrom = maildata["mail_from"]
            _mailto = maildata["mail_to"]
            _context = self.mailcontext

            if _subject and _body:
                # Sending this task to the Celery queue by using .delay.
                send_email_async.delay(
                    _subject,
                    _mailfrom,
                    _mailto,
                    _bodytext,
                    html_body=_bodyhtml,
                    context=_context,
                )
            else:
                app.logger.error(
                    "Could not send E-Mail. Body, Subject or both are invalid."
                )

    def call_uccx(self, uccxdata: dict) -> dict:
        """Process the UCCX action.

        The method will call 'app.curri.uccx.process_ccxrequest()' which
        checks, if the incoming call has a UCCX CTI Port or an agent as
        target.

        Args:
            uccxdata (dict):  Content of the rules 'ldap' action.

        Returns:
            (dict):
                If there is a result for an original calling part
                number, it will be put into a directives information as
                dictionary. Defaults to {}.
        """
        directives = {}
        result = process_ccxrequest(uccxdata, self.requestdict)

        if result["status"] == "retrieved":
            directives = {
                "directives": {
                    "cont": {
                        "modify": {"callingnumber": result["clid"].decode("utf-8")},
                    },
                },
            }
        return directives

    def handle_actions(self, actions):
        """
        High level handling of the actions defined in the matched rule.

        The order in which the different methods are called is important
        since we want to use the modified calling number of the UCCX
        action first. Then, if the LDAP action might overwrite again.
        At the end, the result of all the Transformations is sent out
        via e-mail.

        CAUTION: This needs to be kept in mind when overwriting the
        calling number especially with the LDAP action.

        Args:
            actions (dict): Section 'actions' from the rule as Dictionary.
        """
        app.logger.debug("Handling these actions:")
        prettyout(actions)

        if actions["uccx"]:
            result = self.call_uccx(actions["uccx"])
            if result:
                actions["curri"].update(result)
                self.requestdict["callingnumber"] = result["directives"]["cont"][
                    "modify"
                ]["callingnumber"]
        if actions["ldap"]:
            result = self.call_ldap(actions["ldap"], self.requestdict)
            if result:
                actions["curri"].update(result)
        if actions["mail"]:
            self.call_mailer(actions["mail"])

        self.reply.load_action(actions["curri"])

    def process_request(self, data: str):
        """
        Accept and processes the incoming data from API Path /request.

        Args:
            data (str): CURRI request data.
        """
        self.request.load(data)

        try:
            self.requestdict = self.request.datamodel.dict(by_alias=True)
            self.mailcontext["ecc"] = self.requestdict

            rulename = self.rules.check_ruleset(self.requestdict)
            if rulename is not None:
                self.mailcontext["curri"] = self.rules.get_rule(rulename)
                actions = self.rules.actions
                self.handle_actions(actions)
            else:
                self.reply.set_decision("indeterminate")
        except Exception as err:
            app.logger.error(f"Error during ECC process - {err}")
            app.logger.debug("Traceback", exc_info=True)
            self.reply.set_decision("indeterminate")

        eccreply = self.reply.exportxml()
        app.logger.debug("ECC reply:")
        prettyout(eccreply)

        return self.reply.exportxml()
