from flask_restx import Namespace, Resource

api = Namespace("ldap", description="Collab Curry - LDAP Module API")


@api.route("/")
class ldap_api(Resource):
    @api.response(200, "Success", headers={"Content-Type": "text/html"})
    def get(self):
        return {}
