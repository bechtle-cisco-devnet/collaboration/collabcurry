from typing import Any
from flask import current_app as app
from app import mongo
from pymongo.collection import Collection
from app.ldap.abstract import Ldap2Database
from app.ldap.models import LdapUser

ldapusers: Collection = mongo.db.ldapusers


class Ldap2Mongo(Ldap2Database):
    """Adapter for LDAP related tasks with MongoDB as backend.

    Args:
        Ldap2Database (ABC): Abstract Database object.
    """
    def drop() -> None:
        """Drop the 'ldapusers' collection from MongoDB."""
        ldapusers.drop()

    def get_users(*args: Any) -> list:
        """Get all LDAP users matching the provided arguments.

        Example::

            $ backend = Ldap2Mongo()
            $ backend.find(sAMAccountName='bob', mail='bob@abc.org')

        Returns:
            list: List of dictionaries with the query result.
        """
        app.logger.debug("Reading LDAP user from MongoDB.")
        app.logger.debug()
        result = ldapusers.find(**args)
        return [user.dict() for user in result]

    def save_user(userdata: dict) -> dict:
        """Save data of an LDAP user to MongoDB.

        Args:
            userdata (dict): LDAP userdata as dictionary.

        Returns:
            dict: Userdata as above but including Mongos document id.
        """
        app.logger.debug("Writing LDAP user to MongoDB.")
        user = LdapUser(**userdata)
        result = ldapusers.insert_one(user.to_bson())
        objid = str(result.inserted_id)
        app.logger.debug(f"  ObjectID: {objid}")
        user.id = objid

        return user.dict()
