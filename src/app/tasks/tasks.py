from celery import shared_task
from flask import current_app as app
from app.ldap.api import LdapImport
from app.ldap.database import Ldap2Mongo


@shared_task
def print_hello():
    print("Hello!")


@shared_task
def import_ldap():
    try:
        ldap = LdapImport(Ldap2Mongo)
        ldap.import_users()
    except ValueError as err:
        app.logger.error(f"Could not import LDAP users - {err}")
