"""Create and add the API Blueprint to the flask app."""
from flask import Blueprint
from flask_restx import Api
from app.api.v0.curri import api as curri


bp = Blueprint('api_v0', __name__, url_prefix='/api/v0')

api = Api(bp, title="CURRI REST API", version="1.0")

api.add_namespace(curri)
