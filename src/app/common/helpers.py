"""Several helper functions."""
import os
import re
from typing import Union
from pprint import pformat
import xml.etree.ElementTree as ET
from xml.dom import minidom
from jinja2.exceptions import TemplateNotFound
from flask import current_app as app
from flask import render_template, render_template_string


def check_pattern(pattern: str, data: Union[str, list]) -> bool:
    """
    Validate provided data against a Regex pattern or pattern list.

    Args:
        pattern (str, list): Regex Pattern
        data (str): Provided data to validate.
    """
    app.logger.debug(f"Checking '{data}' against '{pattern}'.")
    if isinstance(pattern, (list)):
        return any(match_dnpattern(item, data) for item in pattern)
    else:
        return match_dnpattern(pattern, data)


def clean_e164(tel: str) -> str:
    """
    Cleanup phone number from special characters or whitespaces.

    Args:
        number (str): Input Phone Number.

    Returns:
        str: Cleaned phone number.
    """
    for x in [" ", ")", "(", "-", "/", "_", "\\"]:
        tel = tel.replace(x, "")

    return tel


def get_environbool(param: str, default: Union[str, bool] = None) -> bool:
    """Get boolean value from OS environment."""
    value = os.environ.get(param, default)

    return value in ["true", "True", True]


def match_dnpattern(pattern: str, data: str):
    """
    Match a provided phone number against a string or Regex pattern.

    Version: 1.0

    Args:
        pattern (str): Simple or Regex pattern to match against.
        data (str): Phone number to check against the pattern.
    """
    app.logger.debug(f"Check pattern '{pattern}' against number '{data}'.")

    # Check if pattern is phone number or RegEx
    if re.fullmatch(r"^(\+|[0-9])+$", pattern):
        app.logger.debug(
            f"  '{pattern}' seems to be a phone number. \
                Running simple match."
        )
        if pattern == data:
            app.logger.debug("    Simple match successful.")
            return True
        else:
            app.logger.debug("    Simple match unsuccessful.")
            return False
    else:
        try:
            regex = re.compile(pattern)
            app.logger.debug(f"  '{pattern}' is a RegEx pattern.")
        except Exception:
            app.logger.debug(f"  '{pattern}' is not a valid RegEx pattern.")
            return False
        result = regex.fullmatch(data)
        if result:
            app.logger.debug(
                f"  Match successful. Pattern:'{pattern}' - " f"Data:'{data}'."
            )
            return True
        else:
            app.logger.debug(
                f"  Match unsuccessful. Pattern:'{pattern}' - " f"Data:'{data}'."
            )
            return False


def match_e164(pattern: str) -> bool:
    """
    Match if a given phone number has a valid +E.164 format.

    Works with not cleaned up +E.164 number (e.g. +49 (1234) 567-890).

    Args:
        pattern (str): Input phone number.

    Returns:
        boolean: True if input has valid +E.164 format, False if not.
    """
    app.logger.debug(f"Trying to match +E.164 number '{pattern}'.")

    pattern = clean_e164(pattern)
    regex = r"(^\+[1-9][0-9]{1,14}$)"
    if re.fullmatch(regex, pattern):
        app.logger.debug(f"  '{pattern}' is in valid +E.164 format.")
        return True
    else:
        app.logger.debug(f" '{pattern}' is not in valid +E.164 format.")
        return False


def match_email(pattern: str) -> bool:
    """
    Match if a given email address has a valid format.

    Args:
        pattern (str): Input email address.

    Returns:
        boolean: True if input has valid email adress format, False if not.
    """
    app.logger.debug(f"Trying to match e-mail address '{pattern}'.")

    regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    if re.fullmatch(regex, pattern):
        app.logger.debug(f"  '{pattern}' is a valid email address.")
        return True
    else:
        app.logger.debug(f"  '{pattern}' is not a valid email address.")
        return False


def match_fqdn(pattern: str) -> bool:
    """
    Try to match an input variable against FQDN specifications.

    * Hostnames are composed of a series of labels concatenated with dots.
    * Each label is 1 to 63 characters long, and may contain: a-z, A-Z, 0-9
      and the hyphen ('-').
    * Additionally: labels cannot start or end with hyphens (RFC 952).
    * Labels can start with numbers (RFC 1123).
    * Trailing dot is not allowed.
    * Max length of ascii hostname including dots is 253 characters
    * TLD (Top Level Domain - last label) is at least 2 characters and only
      ASCII letters
    * At least 1 level above TLD

    Source:  https://stackoverflow.com/questions/11809631
    -------

    Args:
        pattern (str): Input FQDN pattern.

    Returns:
        boolean: True if input has valid FQDN format, False if not.
    """
    app.logger.debug(f"Trying to match FQDN '{pattern}'.")

    regex = r"((?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{0,62}[a-zA-Z0-9]\.)+[a-zA-Z]{2,63}$))"
    if re.fullmatch(regex, pattern):
        app.logger.debug(f"  '{pattern}' is a valid FQDN.")
        return True
    app.logger.debug(f"  '{pattern}' is not a valid FQDN.")
    return False


def match_hostname(pattern: str) -> bool:
    """
    Check if th einput address is in a valid Hostname format.

    Args:
        pattern (str): Input host address.
    """
    app.logger.debug(f"Trying to match Hostname '{pattern}'.")

    regex = r"(?=\A[-a-z0-9]{1,63}\Z)\A[a-z0-9]+(-[a-z0-9]+)*\Z"
    if re.fullmatch(regex, pattern):
        app.logger.debug(f"  '{pattern}' is a valid Hostname.")
        return True
    app.logger.debug(f"  '{pattern}' is not a valid Hostname.")
    return False


def match_hostaddress(pattern: str) -> bool:
    """
    Check if the input address has a valid IP address or FQDN format.

    Args:
        pattern (str): Input host address.

    Returns:
        boolean: True if input has a valid host address, False if not.
    """
    return any([match_ip(pattern), match_hostname(pattern), match_fqdn(pattern)])


def match_ip(pattern: str) -> bool:
    """
    Try to match an input variable against IP address formats.

    Args:
        pattern (str): Input IP address.

    Returns:
        boolean: True if input is a valid IP address, False if not.
    """
    app.logger.debug(f"Trying to match IP address '{pattern}'.")
    regex = re.compile(
        r"^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
        r"\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
        r"\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
        r"\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
    )
    if regex.fullmatch(pattern):
        app.logger.debug(f"  '{pattern}' is in valid IP address format.")
        return True
    else:
        app.logger.debug(f"  '{pattern}' is not in valid IP address format.")
        return False


def match_regex(pattern: str, data: str) -> bool:
    """
    Match a provided string against a Regex pattern.

    Version: 1.0

    Args:
        pattern (str): Regex pattern to match against.
        data (str): Phone number to check against the pattern.
    """
    app.logger.debug(f"Trying to match regex pattern '{pattern}' against '{data}'.")
    try:
        regex = re.compile(pattern)
        app.logger.debug(f"  Pattern '{pattern}' is a regular RegEx pattern.")
    except Exception:
        app.logger.debug(f"  Pattern '{pattern}' is not a valid " "RegEx pattern.")
        return False
    result = regex.fullmatch(data)
    if result:
        app.logger.debug(
            f"  Successful match for Pattern '{pattern}' and value '{data}'."
        )
        return True
    else:
        app.logger.debug(f"  No match for Pattern '{pattern}' and value '{data}'.")
        return False


def prettyout(messyinput: Union[dict, ET.Element, str]) -> None:
    """
    Try to pretty print the provided data.

    Args:
        messyinput (dict, ET.Element, str)
    """
    if isinstance(messyinput, (dict)):
        result = pformat(messyinput, indent=1, compact=True)
    elif isinstance(messyinput, (ET.Element)):
        xmldump = ET.tostring(messyinput)
        dom = minidom.parseString(xmldump)
        result = dom.toprettyxml()
    else:
        result = messyinput

    app.logger.debug(os.linesep + result)


def template_or_str(template: str, context=None) -> str:
    """
    Check if the provided input is a Flask template or just a string.

    Template names are matched against a list of supported file
    extensions. Currently accepted: .txt and .html.

    Args:
        template (str): Input as string.
        context (dict): Context for the template. Default is {}.

    Returns:
        str: Rendered template.
    """
    check_object(template, str, allow_none=False)
    check_object(context, dict, allow_none=True)

    if not context:
        context = {}

    extensions = [".txt", ".html"]
    if any(template.endswith(ext) for ext in extensions):
        try:
            app.logger.debug(
                f"Rendering Template {template} " f"with context {context}."
            )
            result = render_template(template, **context)
            return result
        except TemplateNotFound:
            app.logger.error(f"Template '{template}' does not exist.")
            return None
        except Exception as err:
            app.logger.error(f"Template Rendering - Unknown error: {err}")
            return None
    else:
        result = render_template_string(template, **context)
        return result


def check_object(objs, types, allow_none=True):
    """
    Input object matches provided types.

    Additionally, check if None is allowed.

    Args:
        objs: Input object as tuple or single object.
        types: Allowed input types as tuple or single type.
        allow_none (bol): None allowed, defaults to True.
    """
    if not isinstance(objs, tuple):
        objs = (objs,)

    if not isinstance(types, tuple):
        types = (types,)

    for obj in objs:
        if isinstance(obj, types):
            pass
        elif allow_none and obj is None:
            pass
        else:
            raise TypeError(
                f"Object {obj} is not of type "
                f'{", ".join(t.__name__ for t in types)} '
                f"or not allowed to be None."
            )
