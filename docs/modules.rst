Modules
=======

.. automodule:: app.curri.curri
    :members:

.. automodule:: app.curri.ecc
    :members:

.. automodule:: app.curri.facade
    :members:

.. automodule:: app.curri.models
    :members: