Getting Started
===============

------------
Requirements
------------
The repository is primarily built around Docker. Though it is possible to run the Flask application on it's own after installing all necessary requirements, runnin the environment in Docker is recommended.

Basic Requirements:

* Docker
* Docker-Compose (automatically installed if you are using Docker for Windows)
* Git
* Internet Access for the Setup

-----
Setup
-----

Cloning the repository
-----------------------

Clone the repository from `Gitlab`_.

via SSH

::

    git clone git@gitlab.com:MrCollaborator/collabcurry.git

via HTTPS

::

    git clone https://gitlab.com/MrCollaborator/collabcurry.git


Switch to directory

::

    cd collabcurry

Example Config 
--------------

Copy the example config from /setup/_examples/default to /setup.

Linux / MacOSX

::

    cp -r setup/_examples/. setup/

Windows

::

    cp setup\_examples\* setup\

Edit the files as necessary. 

See chapter :doc:`rules` for further documentation on rules and actions.

See chapter :doc:`environment` for information about environment variables.


Build and start Docker Containers
---------------------------------

Use Docker Compose to build and start the containers. "--parallel" is
optional but faster on multi-core hosts.

::

    docker-compose -f production.yml build --parallel

::

    docker-compose -f production.yml up -d

The container running the Flask application is mapping the /rules directory to read the rules files. This allows for subsequent changes to the rules files during runtime.

Attach to the main container for logging purposes.

::

    docker logs -f collabcurry_curri_prod_1


Configure Cisco UCM ECC Profile
-------------------------------

The URL for Cisco UCMs ECC Profile configuration is:

    http://[collabcurry ip]:80/ecc/request

.. note:: The port ':80' is mandatory. Without, CUCM will not parse the url correctly.


.. _Gitlab: https://gitlab.com/MrCollaborator/collabcurry
